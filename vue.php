<!DOCTYPE html>
<html lang="fr">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta content="utf-8" http-equiv="encoding">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="#" type="image/x-icon">
    <link rel="stylesheet" href="../style.css">
    <script src="https://cdn.jsdelivr.net/npm/marked/marked.min.js" defer></script>
    <script src="../vue.js" defer></script>
    <title>MicroBlog</title>
</head>
<body>

    <h1><?php echo $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] ?></h1>

    <form class="secret-box" method="POST" action="">
        <span class="close" onclick="closeSecret()">&times;</span>
        <label for="secret">Secret</label>
        <input type="password" id="secret" name="secret" autocomplete="off">
        <input type="hidden" name="postContent">
        <input type="hidden" name="blogContent">
        <button>ENVOYER</button>
    </form>

    <div class="left-panel">
        <div class="top-left">
            <p class="followers">123 Abonnés</p>
            <div class="follow">
                <input type="text" id="followBtn" placeholder="blog/url/truc">
                <button id="newBlogBtn" onclick="sendNewBlog()">+</button>
            </div>
        </div>
        <p class="followed">localhost/microblog_php/testvue/vue.php</p>
        <p class="followed">localhost/microblog_php/testvue/vue.php</p>
        <p class="followed">localhost/microblog_php/testvue/vue.php</p>
        <p class="followed">localhost/microblog_php/testvue/vue.php</p>
        <p class="followed">localhost/microblog_php/testvue/vue.php</p>
    </div>

    <div class="blog-content">

        <!-- Nouveau post -->
        <div class="newPost">
            <textarea name="textareaPost" id="textareaPost" placeholder="Ecrivez un nouveau post ..."></textarea>
            <label for="imagePost"><span>&#128206;</span>Envoyer une image</label>
            <input type="file" id="imagePost" name="imagePost" id="imagePost">
            <button type="submit" id="newMessageBtn" onclick="sendNewPost()">Envoyer</button>
        </div>

        <!-- Filtres -->
        <div class="filter">
            <div class="displayFilter">
                <p>Affichage</p>
                <button id="sortBtn">Antechronologique</button>
            </div>
            <div class="loadFilter">
                <p>Chargement</p>
                <button id="loadBtn">Antechronologique</button>
            </div>
        </div>

        <!-- Tous les posts -->
        <div class="allPosts"></div>
    </div>

</body>
</html>