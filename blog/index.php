<?php

error_reporting(E_ALL);
date_default_timezone_set('UTC');

// ---------------- Ajout d'un nouveau post ---------------- //

if($_SERVER['REQUEST_METHOD'] == "POST") {

    if (!empty($_POST['secret'])) {

        $secret = file_get_contents('../DATA/pwd.txt');

        if (hash('sha256', $_POST['secret']) === $secret) {

            if ($_POST['postContent']) {

                // Transformation des données
                $date = time();
                $currentPost = new stdClass();
                $currentPost->url = "http://localhost/microblog_php/DATA/".$date.".json";
                $currentPost->contenu = $_POST['postContent'];
                $currentPost->date = $date;
                
                // Ajout du fichier du post
                file_put_contents("../DATA/".$date.".json", json_encode($currentPost));
                
                // Ajout du post à la liste
                $listFile = file_get_contents("../DATA/posts.json");
                $tempArray = json_decode($listFile);
                array_push($tempArray->post, $currentPost->url);
                file_put_contents("../DATA/posts.json", json_encode($tempArray));
    
                echo $currentPost->url;
                exit();

            } else if ($_POST['blogContent']) {
                
                $listBlogs = file_get_contents("../DATA/subscriptions.json");
                $tempBlogs = json_decode($listBlogs);
                array_push($tempBlogs->subscriptions, $_POST['blogContent']);
                file_put_contents("../DATA/subscriptions.json", json_encode($tempBlogs));

                http_response_code(200);

            }

        } else {
            header('HTTP/1.0 403 Forbidden');
        }
    } else {
        header('HTTP/1.0 403 Forbidden');
    }
}

// ---------------- Affichage des posts ---------------- //

include('../vue.php');

?>