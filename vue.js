// ---------- Configuration -----------------------------------------------------------------------


var config = {
    // Fichier contenant le liste (path.json)
    uri: '../DATA/posts.json',
    // Ordre de chargement (asc | desc)
    load: 'desc',
    // Ordre d'affichage' (asc | desc)
    sort: 'desc',
    // Timer d'auto-update (int [en ms])
    timer: 10000,
    // Fichier contenant la liste des autres blogs (path.json)
    subs: '../DATA/subscriptions.json'
};


// ---------- DOM ---------------------------------------------------------------------------------


blogContent = document.querySelector('.allPosts');

var sortingButton = document.querySelector('#sortBtn');
var loadingButton = document.querySelector('#loadBtn');
var sort = sortingButton.addEventListener("click", () => changeOrder(sortingButton));
var load = loadingButton.addEventListener("click", () => changeOrder(loadingButton));

var actualList = [];

var textPost = document.querySelector('#textareaPost');
var imagePost = document.querySelector('#imagePost');

var toFollow = document.querySelector('#followBtn');


// ---------- Afficher posts ----------------------------------------------------------------------

// Va chercher la liste des posts (Async)
function fetchList (link, orderLoad, orderDisplay) {
    fetch(link, {cache: 'no-cache'})
        .then(res => res.json())
        .then(list => {
            // Fetch de la nouvelle liste
            newList = sortList(list.post, orderLoad);
            // Liste temporaire contenant la différence à ajouter
            tempList = compareLists(newList, actualList);
            fetchPost(tempList, orderDisplay);
            // Actualisation et réorganisation de la liste
            actualList = actualList.concat(tempList);
            actualList = sortList(actualList, orderLoad);
        })
}

// Trie la liste des posts (Sync)
function sortList (list, order) {
    if (order == 'asc') {
        list.sort((a,b) => (a.date > b.date) ? 1 : ((b.date > a.date) ? -1 : 0));
    } else if (order == 'desc') {
        list.sort((a,b) => (a.date < b.date) ? 1 : ((b.date < a.date) ? -1 : 0));
    }

    return list;
}

// Va chercher tous les posts (Async)
function fetchPost (list, order) {
    for (post of list) {
        fetch(post)
            .then(res => res.json())
            .then(res => createPost(res, order))
    }
}

// Crée  et affiche les posts (Sync)
function createPost (obj, order) {
    div = document.createElement("div");
    div.className = "post";
    if (order == 'asc') {
        div.style.order = obj.date;
    } else if (order == 'desc') {
        div.style.order = obj.date * -1;
    }

    date = document.createElement("p");
    url = document.createElement("p");
    content = document.createElement("p");
    
    date.className = "post-date";
    url.className = "post-url";
    content.className = "post-content";

    date.textContent = obj.date;
    url.textContent = obj.url;
    content.innerHTML = marked.parseInline(obj.contenu, { gfm: true, breaks: true });

    div.append(date, url, content);
    blogContent.append(div);
}

// Switche l'ordre de chargement et affichage (Sync)
function changeOrder (button) {
    actualList = [];

    if (button.textContent == 'Antechronologique') {

        button.textContent = 'Chronologique';
        button === sortingButton ? config.sort = 'asc' : config.load = 'asc';

    } else {

        button.textContent = 'Antechronologique';
        button === sortingButton ? config.sort = 'desc' : config.load = 'desc';

    }
    
    blogContent.innerText = '';
    fetchList(config.uri, config.load, config.sort);

}

// Comparer les deux listes
function compareLists (newList, oldList) {
    return newList.filter((content) => !oldList.includes(content));
}

// Reload l'affichage des posts
function reload () {
    fetchList(config.uri, config.load, config.sort);
    setTimeout(reload, config.timer);
}

reload();


// ---------- Nouveau post / blog -----------------------------------------------------------------


// Masque le champ pour entrer le secret
function closeSecret() {
    document.querySelector('.secret-box').style.display = "none";
}

// Affiche le champ pour entrer le secret
function showSecret(toSend) {
    document.querySelector('.secret-box').style.display = "flex";
    if (toSend.post) {
        document.querySelector('.secret-box input[name=postContent]').value = toSend.post;
    } else if (toSend.blog) {
        document.querySelector('.secret-box input[name=blogContent]').value = toSend.blog;
    }
}

// Envoi un nouveau post
function sendNewPost() {

    if(!textPost.value) {

        textPost.style.animation = "error 300ms linear 2";
        setTimeout(() => {
            textPost.style.animation = "";
        }, 800);

    } else {

        tempPost = textPost.value;
        showSecret({post: tempPost});

    }

}

// Envoi un nouveau blog
function sendNewBlog() {

    if(!toFollow.value) {

        toFollow.style.animation = "error 300ms linear 2";
        setTimeout(() => {
            toFollow.style.animation = "";
        }, 800);

    } else {
        
        tempBlog = toFollow.value;
        regex = /(?:https?:\/\/)?(?:www\.|\/)?(\w+)(\/|\.|\R)?/;
        tempUrl = tempBlog.match(regex);
        tempUrl = "http://localhost/" + tempUrl[1] + "/";
        console.log(tempUrl);
        httpGet(tempUrl + "subscribe/", () => {
            showSecret({blog: tempUrl});
        });

    }

}

// Requette GET
function httpGet(url, callback) {
    let http = new XMLHttpRequest();
    http.onreadystatechange = () => {
        if (http.readyState == 4 && http.status == 200) {
            callback(http.responseText);
        } else if (http.readyState == 4 && http.status != 200) {
            displayError();
        }
    }
    http.open('GET', url, true);
    http.send(null);
}

// Affiche le champ d'erreur
function displayError() {
    let p = document.createElement('div');
    p.classList.add("error-box");
    p.innerText = "URL invalide !";
    let s = document.createElement('span');
    s.classList.add('close');
    s.innerHTML = "&times;"
    s.setAttribute('onclick', 'closeSecret()');
    p.appendChild(s);
    document.body.appendChild(p);
}

// Masque le champ d'erreur
function closeSecret() {
    document.querySelector('.error-box').remove();
}